import { JwtHelperService } from '@auth0/angular-jwt';
import { TOKEN_NAME } from './../../_shared/var.constant';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario : string;
  roles : string[];

  constructor() { }

  ngOnInit() {

    const helper = new JwtHelperService();

    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken = helper.decodeToken(tk.access_token);
    this.usuario = decodedToken.user_name;
    this.roles = decodedToken.authorities;
  }

}
