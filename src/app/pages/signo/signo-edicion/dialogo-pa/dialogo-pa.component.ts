import { PacienteService } from 'src/app/_service/paciente.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ɵConsole, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';

@Component({
  selector: 'app-dialogo-pa',
  templateUrl: './dialogo-pa.component.html',
  styleUrls: ['./dialogo-pa.component.css']
})
export class DialogoPaComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<DialogoPaComponent>, @Inject(MAT_DIALOG_DATA) public data: Paciente, private pacienteService: PacienteService) { }

  paciente : Paciente;

  ngOnInit() {
    this.paciente = new Paciente();
    this.paciente.nombres = this.data.nombres;
    this.paciente.apellidos = this.data.apellidos;
    this.paciente.email = this.data.email;
    this.paciente.telefono = this.data.telefono;
    this.paciente.dni = this.data.dni;
  }

  cancelar() {
    this.dialogRef.close();
  }

  operar() {
    this.pacienteService.registrar(this.paciente).subscribe(data => {
        this.pacienteService.listar().subscribe(pacientes => {
          this.pacienteService.pacienteCambio.next(pacientes);
          this.pacienteService.mensajeCambio.next("Se registro");
        });
      });
    
    this.dialogRef.close();
  }

}
