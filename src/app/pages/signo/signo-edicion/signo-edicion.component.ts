import { DialogoPaComponent } from './dialogo-pa/dialogo-pa.component';
import { map } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Signo } from 'src/app/_model/signo';
import { SignoService } from 'src/app/_service/signo.service';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  id: number;
  form: FormGroup;
  edicion: boolean = false;
  signo: Signo;

  myControlPaciente: FormControl = new FormControl();

  pacientes: Paciente[] = [];

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date()

  mensaje: string;

  filteredOptions: Observable<any[]>;

  pacienteSeleccionado: Paciente;

  constructor(private route: ActivatedRoute, private router: Router, private signoService: SignoService, private builder: FormBuilder, private pacienteService: PacienteService, public snackBar: MatSnackBar, private dialog: MatDialog) { }

  ngOnInit() {
    this.form = new FormGroup({
      'idSigno': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });
    this.signo = new Signo();
    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;

      this.initForm();
    })
  }

  initForm() {
    if (this.edicion) {
      //cargar la data del servicio hacia el form 
      
      this.signoService.listarSignoPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'idSigno': new FormControl(data.idSigno),
          'paciente': new FormControl(data.paciente),
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
        });
        console.log(data);
      });
    }
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }



  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  seleccionarPaciente(e: any){
    console.log(e);
    this.pacienteSeleccionado = e.option.value;
  }

  estadoBotonRegistrar() {
    return (this.pacienteSeleccionado === null);
  }

  operar() {
    this.signo = new Signo();
    this.signo.idSigno = this.form.value['idSigno'];
    this.signo.paciente = this.form.value['paciente'];//this.pacienteSeleccionado;
    //var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
    //var localISOTime = (new Date(Date.now() - tzoffset)).toISOString()
    //this.signo.fecha = localISOTime;
    this.signo.fecha = this.form.value['fecha'].toISOString();
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    console.log(this.signo)

    if (this.edicion) {
      //actualizar
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se modificó');
        });
      });
    } else {
      //registrar
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se registró');
        });
      });
    }

    this.router.navigate(['signo']);
  }

  limpiarControles() {
    this.pacienteSeleccionado = null;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
    this.signo = new Signo();
  }

  openDialog() {
    let pac =  new Paciente();
    const dialogRef = this.dialog.open(DialogoPaComponent, {
      width: '250px',
      disableClose: false,
      data : pac
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Prueba de accion despues de cerrar');
      this.listarPacientes();
    });
  }

}
